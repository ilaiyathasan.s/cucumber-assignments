package stepDefinitions;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(tags = "", features = "src/test/resources/features/logintestng.feature", 
glue = "stepDefinitions",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)
public class runnerClassTestNGDemoWebSHop extends AbstractTestNGCucumberTests {

}
