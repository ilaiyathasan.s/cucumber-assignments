package stepDefinitions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class tesngStepsDemoWebShopLogin {

	WebDriver driver;
	@Given("open the browser")
	public void open_the_browser() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	}

	@Then("homepage of the website gets displayed")
	public void homepage_of_the_website_gets_displayed() {
		driver.manage().window().maximize();
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("enter the email {string} in email textbox")
	public void enter_the_email_in_email_textbox(String username) {
		driver.findElement(By.id("Email")).sendKeys(username);
	}

	@When("enter the password {string} in password textbox")
	public void enter_the_password_in_password_textbox(String password) {
		driver.findElement(By.id("Password")).sendKeys(password);
	}

	@Then("user get logged in successfully")
	public void user_get_logged_in_successfully() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("close the browser")
	public void close_the_browser() {
	    driver.quit();
	}
}
