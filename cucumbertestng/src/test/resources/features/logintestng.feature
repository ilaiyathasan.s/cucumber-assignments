Feature: Demo Web Shop

  Scenario Outline: Verify the demowebshop login funtionality
    Given open the browser
    Then homepage of the website gets displayed
    When click on the login button
    And enter the email "<username>" in email textbox
    And enter the password "<password>" in password textbox
    And click on the login button
    Then user get logged in successfully
    And close the browser

    Examples: 
      | username                 | password |
      | misterthasan24@gmail.com |   123456 |
      | ramkumar24@gmail.com     | ram@123  |
