package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoWebShopLoginStep {
 WebDriver driver;
	@Given("open the browser")
	public void open_the_browser() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//System.out.println("Browser gets launched");
	}

	@Then("homepage of the website gets displayed")
	public void homepage_of_the_website_gets_displayed() {
		
	  // System.out.println("Homepage gets appeared");
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	    //System.out.println("Login button gets clicked");
	}

	@When("enter the email in email textbox")
	public void enter_the_email_in_email_textbox() {
		driver.findElement(By.id("Email")).sendKeys("ramkumar24@gmail.com");
		//System.out.println("Enter valid email in the email textbox");
	   	}

	@When("enter the password in password textbox")
	public void enter_the_password_in_password_textbox() {
		driver.findElement(By.id("Password")).sendKeys("ram@123");
	    //System.out.println("Enter valid password in the password textbox");
	}

	@Then("user get logged in successfully")
	public void user_get_logged_in_successfully() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		//System.out.println("User gets logged in successfully");
	    
	}

	@Then("close the browser")
	public void close_the_browser() {
		//driver.findElement(By.linkText("Log out")).click();
		driver.quit();
	  // System.out.println("Closes the browser");
	}


}
