
Feature: Demo Web Shop

  Scenario: Verify the demowebshop login funtionality
    Given open the browser 
    Then homepage of the website gets displayed
    When click on the login button
    And enter the email in email textbox
    And enter the password in password textbox
    And click on the login button
    Then user get logged in successfully
    And close the browser

  #@tag2
  #Scenario Outline: Title of your scenario outline
    #Given I want to write a step with <name>
    #When I check for the <value> in step
    #Then I verify the <status> in step
#
    #Examples: 
      #| name  | value | status  |
      #| name1 |     5 | success |
      #| name2 |     7 | Fail    |
